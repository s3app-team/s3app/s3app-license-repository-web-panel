import { gql, useMutation, useQuery } from '@apollo/client';
import { Button } from '@mui/material';
import moment from 'moment';
import { DeleteOutlined } from '@mui/icons-material';
import LoadingCircle from '../../components/loading-circle';

const GET_ORDERS = gql`
  query {
    me {
      orders {
        id
        isPaid
        paidAt
        createdAt
        elements {
          id
          name
          count
          product {
            id
            name
            price
            days
          }
          licenseKey {
            id
            name
            expireAt
          }
        }
        payments {
          id
          status
          createdAt
          formLink
          paidAt
        }
      }
    }
  }
`;

const BUY_ORDER = gql`
  mutation($buyOrderId: ID!) {
    buyOrder(id: $buyOrderId) {
      id
      formLink
    }
  }
`;

const DELETE_ORDER = gql`
  mutation($Id: ID!) {
    deleteOrder(id: $Id)
 }
`;

function Orders(props) {
  const { data, loading, refetch } = useQuery(GET_ORDERS);
  const [buyOrder] = useMutation(BUY_ORDER);
  const [deleteOrder] = useMutation(DELETE_ORDER);

  if (loading) return <LoadingCircle />;

  const ordersSorted = [...data.me.orders];
  ordersSorted.sort((order1, order2) => (order1.createdAt > order2.createdAt ? -1 : 1));
  console.log(ordersSorted);
  return (
    <div className="size-full">
      <h2 className="font-medium text-xl text-black/60 mb-3.5 uppercase">ЗАКАЗЫ</h2>
      {ordersSorted.length ? (
        <div className="flex flex-col gap-5">
          {ordersSorted.map((order) => {
            let sum = 0;
            order.elements.forEach((element) => {
              sum += element.product.price * element.count;
            });
            return (
              <div className="flex flex-col gap-5 bg-white rounded p-6 shadow-lg" key={order.id}>
                <span className="text-black/60 text-lg">
                  {`Номер заказа: ${order.id}`}
                </span>
                {order.elements.map((element) => (
                  <div
                    className="flex justify-between items-center bg-zinc-100 rounded p-2.5 shadow-lg"
                    key={element.id}
                  >
                    <div className="flex items-center gap-2">
                      {/* TODO: Заменить на изображение товара */}
                      <div
                        className="size-32 bg-blue-200 rounded shadow-lg flex items-center justify-center text-xl"
                      >
                        <span>S3APP</span>
                      </div>
                      <div>
                        <div className="flex flex-col justify-between">
                          <div className="flex flex-col">
                            <span className="text-2xl font-semibold">{element.product.name}</span>
                            {/* TODO: Добавить описание товара */}
                            {/* <span className="text-xl text-black/60 font-light">
                            Описание</span> */}
                          </div>
                          <div className="mt-4">
                            <span className="text-2xl text-black/75 font-semibold">
                              {element.product.price}
                              {' '}
                              ₽
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="flex flex-col items-end">
                      <span className="text-primary font-medium text-lg">
                        {element.licenseKey ? 'ПРОДЛЕНИЕ' : 'НОВЫЙ'}
                      </span>
                      <span className="text-xl">
                        Ключ:
                        {' '}
                        {element?.name ? element.name : 'Безымянный ключ'}
                      </span>
                      <span className="font-medium text-black/60 text-lg">
                        {element.count}
                        {' '}
                        шт
                        (
                        {element.count * element.product.days}
                        {' '}
                        дней
                        {' / '}
                        До:
                        {' '}
                        {moment(element.licenseKey?.expireAt
                          ? new Date(element.licenseKey.expireAt).getTime()
                                + element.count * element.product.days * 24 * 60 * 60 * 1000
                          : new Date().getTime()
                                + element.count * element.product.days * 24 * 60 * 60 * 1000).format('DD.MM.YYYY')}
                        )
                      </span>
                    </div>
                  </div>
                ))}
                <div className="flex items-center justify-end gap-4">
                  <span className="text-xl font-bold">
                    {`${order.isPaid ? 'ОПЛАЧЕНО: ' : 'ИТОГО: '} ${sum} ₽`}
                  </span>
                  {!order.isPaid
                  && (
                  <>
                    <Button
                      className="normal-case bg-primary/20 text-primary text-lg font-normal"
                      size="large"
                      variant="contained"
                      onClick={async () => {
                        const payment = await buyOrder({
                          variables: {
                            buyOrderId: order.id,
                          },
                        });
                        window.open(payment.data.buyOrder.formLink);
                      }}
                    >
                      Заказать
                    </Button>
                    <Button
                      className="min-w-0 size-11"
                      color="inherit"
                      variant="contained"
                      onClick={async () => {
                        await deleteOrder({
                          variables: {
                            Id: order.id,
                          },
                        });
                        props.refetchUser();
                        refetch();
                      }}
                    >
                      <DeleteOutlined />
                    </Button>
                  </>
                  )}
                </div>
              </div>
            );
          })}
        </div>
      )
        : (
          <div className="size-full flex flex-col items-center justify-center">
            <span
              className="text-4xl uppercase text-black/40 font-medium"
            >
              Заказов нет
            </span>
          </div>
        )}
    </div>
  );
}

export default Orders;
