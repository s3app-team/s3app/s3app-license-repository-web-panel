import { gql, useQuery } from '@apollo/client';
import LoadingCircle from '../../../components/loading-circle';

const GET_ORDERS = gql`
query {
    getOrders {
        id
        elements {
            count
            product {
                id
                name
                price
            }
        }
    }
}
`;

function Orders() {
  const { data, loading } = useQuery(GET_ORDERS);

  if (loading) return <LoadingCircle />;

  return (
    <div>
      {data.getOrders.map((order) => <div key={order.id}>{order.name}</div>)}
    </div>
  );
}

export default Orders;
