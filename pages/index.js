import Products from './products';

function Home(props) {
  return <Products {...props} />;
}

export default Home;
