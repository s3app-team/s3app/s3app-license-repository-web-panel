import { gql, useMutation } from '@apollo/client';
import { Button, TextField } from '@mui/material';
import { useRouter } from 'next/router';
import { useState } from 'react';

const ADD_PRODUCT = gql`
  mutation($input: ProductInput!) {
    addProduct(input: $input) {
      id
    }
  }
`;

function CreateProduct() {
  const [addProduct] = useMutation(ADD_PRODUCT);
  const [data, setData] = useState({ name: '', price: 0, days: 1 });
  const router = useRouter();

  return (
    <div>
      <div>
        <TextField
          label="Название"
          value={data.name}
          onChange={(e) => setData({ ...data, name: e.target.value })}
          variant="standard"
        />
      </div>
      <div>
        <TextField
          label="Цена"
          value={data.price}
          onChange={(e) => setData({ ...data, price: parseInt(e.target.value, 10) })}
          variant="standard"
        />
      </div>
      <div>
        <TextField
          label="Количество дней"
          value={data.days}
          onChange={(e) => setData({ ...data, days: parseInt(e.target.value, 10) })}
          variant="standard"
        />
      </div>
      <div>
        <Button onClick={async () => {
          await addProduct({
            variables: {
              input: data,
            },
          });
          router.push('/products');
        }}
        >
          Создать
        </Button>
      </div>
    </div>
  );
}

export default CreateProduct;
