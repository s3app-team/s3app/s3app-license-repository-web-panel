import { gql, useMutation, useQuery } from '@apollo/client';
import { Button } from '@mui/material';
import Link from 'next/link';
import { useSnackbar } from 'notistack';
import Image from 'next/image';
import LoadingCircle from '../../components/loading-circle';

const GET_PRODUCTS = gql`
query {
    getProducts {
        id
        name
        price
        days
    }
}
`;

const ADD_CART_ELEMENT = gql`
  mutation($input: CartElementInput!) {
    addCartElement(input: $input) {
      id
    }
  }
`;

function Products(props) {
  const { data, loading } = useQuery(GET_PRODUCTS);
  const [addCartElement] = useMutation(ADD_CART_ELEMENT);
  const { enqueueSnackbar } = useSnackbar();

  if (loading) return <LoadingCircle />;

  return (
    <div>
      <div>
        {props.user?.role === 'admin' ? (
          <Link href="/products/create">
            <Button variant="contained">
              Создать продукт
            </Button>
          </Link>
        ) : null}
      </div>
      <h2 className="font-medium text-xl text-black/60 mb-3.5 uppercase">Товары</h2>
      <div className="flex flex-col gap-5">
        {data.getProducts.map((product) => (
          <div className="flex justify-between items-center bg-white rounded p-2.5 shadow-lg" key={product.id}>
            <div className="flex items-center gap-2">
              {/* TODO: Заменить на изображение товара */}
              <div className="size-32 bg-blue-200 rounded shadow-lg flex items-center justify-center text-xl">
                <span>S3APP</span>
              </div>
              <div>
                <div className="flex flex-col justify-between">
                  <div className="flex flex-col">
                    <span className="text-2xl font-semibold">{product.name}</span>
                    {/* TODO: Добавить описание товара */}
                    {/* <span className="text-xl text-black/60 font-light">Описание</span> */}
                  </div>
                  <div className="mt-4">
                    <span className="text-2xl text-black/75 font-semibold">
                      {product.price}
                      {' '}
                      ₽
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <Button
                className="font-normal normal-case bg-primary/20 text-primary mr-4"
                variant="contained"
                size="large"
                onClick={async () => {
                  if (props.user) {
                    await addCartElement({
                      variables: {
                        input: {
                          productId: product.id,
                          count: 1,
                        },
                      },
                    });
                    props.refetchUser();
                    enqueueSnackbar('Товар добавлен в корзину', { variant: 'success' });
                  } else {
                    window.location.href = '/login';
                  }
                }}
              >
                Добавить в корзину
                <Image className="ml-2" src="cart.svg" alt="cart_icon" width={32} height={32} />
              </Button>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Products;
