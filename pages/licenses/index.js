import { gql, useMutation, useQuery } from '@apollo/client';
import {
  CachedRounded, ContentCopyOutlined, MailOutlined, Save,
} from '@mui/icons-material';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  IconButton,
  TextField,
  Tooltip,
} from '@mui/material';
import copy from 'copy-to-clipboard';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import { useEffect, useState } from 'react';
import clsx from 'clsx';
import LoadingCircle from '../../components/loading-circle';

const GET_LICENSES = gql`
  query {
    me {
      licenses {
        id
        licenseKey
        activated
        expireAt
        name
        activatedAt
        product {
          id
          name
        }
      }
    }
  }
`;

const ADD_CART_ELEMENT = gql`
  mutation($input: CartElementInput!) {
    addCartElement(input: $input) {
      id
    }
  }
`;

const RENAME_LICENSE = gql`
    mutation($renameLicenseId: ID!, $name: String!) {
        renameLicenseKey(licenseKeyId: $renameLicenseId, name: $name) {
            id
        }
    }
`;

const SEND_LICENSE_KEY_EMAIL = gql`
    mutation($licenseKeyId: ID!, $email: String!) {
        sendLicenseKeyEmail(licenseKeyId: $licenseKeyId, email: $email)
    }
`;

const REFRESH_LICENSE_KEY = gql`
  mutation($licenseKeyId: ID!) {
    refreshLicenseKey(licenseKeyId: $licenseKeyId) {
      id
      licenseKey
    }
  }
`;

function SendDialog(props) {
  const [sendLicenseKeyEmail] = useMutation(SEND_LICENSE_KEY_EMAIL);
  const { enqueueSnackbar } = useSnackbar();
  const [email, setEmail] = useState('');
  useEffect(() => {
    setEmail(props.email);
  }, [props.open, props.email]);

  return (
    <Dialog open={props.open} onClose={() => props.setSendDialog(false)} fullWidth>
      <DialogTitle>Отправить на почту</DialogTitle>
      <DialogContent>
        <TextField
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          label="Email"
          variant="standard"
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button
          onClick={async () => {
            await sendLicenseKeyEmail({
              variables: {
                licenseKeyId: props.sendDialog.id,
                email,
              },
            });
            enqueueSnackbar('Ключ отправлен на почту', { variant: 'success' });
            props.refetch();
            props.setSendDialog(false);
          }}
        >
          Отправить
        </Button>
        <Button onClick={() => props.setSendDialog(false)}>Отмена</Button>
      </DialogActions>
    </Dialog>
  );
}

function RefreshDialog(props) {
  const [refreshLicenseKey] = useMutation(REFRESH_LICENSE_KEY);
  const { enqueueSnackbar } = useSnackbar();

  return (
    <Dialog open={props.open} onClose={() => props.setRefreshDialog(false)}>
      <DialogTitle>Сбросить ключ</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Вы действительно хотите сбросить ключ? Это лишит доступа тех, кто его уже использует.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={async () => {
            await refreshLicenseKey({
              variables: {
                licenseKeyId: props.refreshDialog.id,
              },
            });
            await props.refetch();
            enqueueSnackbar('Ключ сброшен', { variant: 'success' });
            props.setRefreshDialog(false);
          }}
        >
          Сбросить
        </Button>
        <Button onClick={() => props.setRefreshDialog(false)}>Отмена</Button>
      </DialogActions>
    </Dialog>
  );
}

function RenameLicense(props) {
  const [renameLicense] = useMutation(RENAME_LICENSE);
  const [name, setName] = useState(props.name || '');

  useEffect(() => {
    setName(props.name);
  }, [props.name]);

  return (
    <div>
      <TextField
        variant="filled"
        size="small"
        label="Название"
        value={name || 'Безымянный ключ'}
        onChange={(e) => {
          setName(e.target.value);
        }}
      />
      {props.name !== name && (
      <IconButton
        size="small"
        onClick={async () => {
          await renameLicense({
            variables: {
              renameLicenseId: props.id,
              name,
            },
          });
          props.refetch();
        }}
      >
        <Save />
      </IconButton>
      )}
    </div>
  );
}

function Licenses(props) {
  const { data, loading, refetch } = useQuery(GET_LICENSES);
  const [addCartElement] = useMutation(ADD_CART_ELEMENT);

  const { enqueueSnackbar } = useSnackbar();

  const [sendDialog, setSendDialog] = useState(false);
  const [refreshDialog, setRefreshDialog] = useState(false);

  if (loading) return <LoadingCircle />;

  return (
    <div className="size-full">
      <h2 className="font-medium text-xl text-black/60 mb-3.5 uppercase">Ключи</h2>
      <SendDialog
        setSendDialog={setSendDialog}
        refetch={refetch}
        email={sendDialog?.email || ''}
        open={!!sendDialog}
        sendDialog={sendDialog}
      />
      <RefreshDialog
        setRefreshDialog={setRefreshDialog}
        refetch={refetch}
        open={!!refreshDialog}
        refreshDialog={refreshDialog}
      />
      <div
        className="flex items-center bg-white rounded p-6 shadow-lg mb-3 text-base text-black/60 font-medium"
      >
        <span>
          Всего ключей:
          {' '}
          {data.me.licenses.length}
        </span>
        <Divider className="mx-2 border" flexItem orientation="vertical" />
        <span>
          Активированных ключей:
          {' '}
          {data.me.licenses.filter((license) => license.activated).length}
        </span>
        <Divider className="mx-2 border" flexItem orientation="vertical" />

        <span>
          Неактивированных ключей:
          {' '}
          {data.me.licenses.filter((license) => !license.activated).length}
        </span>
      </div>
      <div className="flex flex-col gap-4">
        {data.me.licenses.map((license) => (
          <div
            className="flex justify-between items-center bg-zinc-100 rounded p-2.5 shadow-lg"
            key={license.id}
          >
            <div className="flex items-center gap-2">
              {/* TODO: Заменить на изображение товара */}
              <div
                className="size-32 bg-blue-200 rounded shadow-lg flex items-center justify-center text-xl"
              >
                <span>S3APP</span>
              </div>
              <div>
                <div className="flex flex-col justify-between">
                  <div className="flex flex-col">
                    <span className="text-2xl font-semibold">{license.product.name}</span>
                    {/* TODO: Добавить описание товара */}
                    {/* <span className="text-xl text-black/60 font-light">Описание</span> */}
                  </div>
                  <div className="mt-4">
                    <span className={clsx({ 'text-primary': license.activated }, 'text-2xl text-black/75 font-semibold')}>
                      {license.activated ? 'Активирован' : 'Не активирован'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex items-center gap-7 mr-4">
              <div className="flex-col">
                <RenameLicense id={license.id} name={license.name} refetch={refetch} />
                <div className="flex items-center justify-between mt-2.5 text-black/60 text-base">
                  <span>
                    До
                    {' '}
                    {moment(license?.expireAt).format('DD.MM.YYYY HH:MM:SS')}
                  </span>
                </div>
              </div>
              <div className="flex flex-col gap-2">
                <Button
                  variant="contained"
                  color="inherit"
                  className="text-black/60 justify-end"
                  onClick={() => {
                    setSendDialog(license);
                  }}
                >
                  Отправить на почту
                  <MailOutlined className="ml-2" />
                </Button>
                <Button
                  variant="contained"
                  color="inherit"
                  className="text-black/60 justify-end"
                  onClick={() => {
                    copy(license.licenseKey);
                    enqueueSnackbar('Ключ скопирован в буфер обмена', { variant: 'success' });
                  }}
                >
                  Скопировать
                  <ContentCopyOutlined className="ml-2" />
                </Button>
              </div>
              <Button
                className="normal-case bg-primary/20 text-primary text-lg font-normal"
                size="large"
                variant="contained"
                onClick={async () => {
                  await addCartElement({
                    variables: {
                      input: {
                        licenseKeyId: license.id,
                        count: 1,
                      },
                    },
                  });
                  props.refetchUser();
                  enqueueSnackbar('Товар добавлен в корзину', { variant: 'success' });
                }}
              >
                Продлить
              </Button>
              <Tooltip title="Сбросить ключ">
                <Button
                  className="min-w-0 size-11 text-black/60"
                  onClick={async () => {
                    setRefreshDialog(license);
                  }}
                  color="inherit"
                  variant="contained"
                >
                  <CachedRounded />
                </Button>
              </Tooltip>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Licenses;
