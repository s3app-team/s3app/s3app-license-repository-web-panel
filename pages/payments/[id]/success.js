import { gql, useQuery } from '@apollo/client';
import { useRouter } from 'next/router';
import { TaskAltRounded } from '@mui/icons-material';
import { Button } from '@mui/material';
import LoadingCircle from '../../../components/loading-circle';

const GET_PAYMENT = gql`
    query($id: ID!) {
        getPayment(id: $id) {
            id
            status
            order {
                id
                sum
            }
        }
    }
`;

function Success() {
  const router = useRouter();
  const { data, loading } = useQuery(GET_PAYMENT, {
    variables: {
      id: router.query.id,
    },
  });

  if (loading) {
    return <LoadingCircle />;
  }

  return (
    <div className="size-full flex flex-col items-center justify-center">
      <TaskAltRounded className="size-20 fill-green-500" />
      <h1 className="text-xl font-medium">ЗАКАЗ УСПЕШНО ОПЛАЧЕН</h1>
      <span className="text-black/60 text-lg">
        Номер заказа:
        {' '}
        {data.getPayment.order.id}
      </span>
      <Button
        className="normal-case bg-primary/20 text-primary text-lg font-normal mt-3"
        href="/licenses"
        size="large"
        variant="contained"
      >
        Перейти к списку ключей
      </Button>
    </div>
  );
}

export default Success;
