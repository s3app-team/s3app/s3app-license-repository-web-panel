import { gql, useQuery } from '@apollo/client';
import { useRouter } from 'next/router';
import { ErrorOutlined } from '@mui/icons-material';
import { Button } from '@mui/material';
import LoadingCircle from '../../../components/loading-circle';

const GET_PAYMENT = gql`
    query($id: ID!) {
        getPayment(id: $id) {
            id
            status
            order {
                id
                sum
            }
        }
    }
`;

function Error() {
  const router = useRouter();
  const { data, loading } = useQuery(GET_PAYMENT, {
    variables: {
      id: router.query.id,
    },
  });

  if (loading) {
    return <LoadingCircle />;
  }

  return (
    <div className="size-full flex flex-col items-center justify-center">
      <ErrorOutlined className="size-20 fill-red-500" />
      <h1 className="text-xl font-medium">ОШИБКА ПРИ ОПЛАТЕ ЗАКАЗА</h1>
      <span className="text-black/60 text-lg">
        Номер заказа:
        {' '}
        {data.getPayment.order.id}
      </span>
      <Button
        className="normal-case bg-primary/20 text-primary text-lg font-normal mt-3"
        href="/orders"
        size="large"
        variant="contained"
      >
        Перейти к списку заказов
      </Button>
    </div>
  );
}

export default Error;
