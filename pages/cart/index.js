import { gql, useMutation, useQuery } from '@apollo/client';
import { Add, DeleteOutlined, Remove } from '@mui/icons-material';
import { Button, TextField } from '@mui/material';
import moment from 'moment';
import { useRouter } from 'next/router';
import LoadingCircle from '../../components/loading-circle';

const GET_CART = gql`
  query {
    me {
      cart {
        id
        count
        name
        createdAt
        licenseKey {
          id
          expireAt
          activated
          name
        }
        product {
          id
          name
          days
          price
        }
      }
    }
  }`;

const EDIT_CART_ELEMENT = gql`
  mutation($editCartElementId: ID!, $input: CartElementUpdateInput!) {
    editCartElement(id: $editCartElementId, input: $input) {
      id
    }
  }
`;

const REMOVE_CART_ELEMENT = gql`
  mutation($removeCartElementId: ID!) {
    removeCartElement(id: $removeCartElementId)
  }
`;

const CLEAR_CART = gql`
  mutation {
    clearCart
  }
`;

const ORDER_CART = gql`
    mutation {
        orderCart {
            id
        }
    }
`;

function Cart(props) {
  const { data, loading, refetch } = useQuery(GET_CART);
  const [editCartElement] = useMutation(EDIT_CART_ELEMENT);
  const [removeCartElement] = useMutation(REMOVE_CART_ELEMENT);
  const [clearCart] = useMutation(CLEAR_CART);
  const [orderCart] = useMutation(ORDER_CART);

  const router = useRouter();

  const changeElement = async (id, input) => {
    await editCartElement({
      variables: {
        editCartElementId: id,
        input,
      },
    });
    refetch();
  };

  if (loading) {
    return <LoadingCircle />;
  }
  return (
    <div className="size-full">
      <h2 className="font-medium text-xl text-black/60 mb-3.5 uppercase">Корзина</h2>
      {data.me.cart.length ? (
        <div className="flex flex-col gap-5 bg-white rounded p-6 shadow-lg">
          {data.me.cart.map((cart, index) => (
            <div className="flex justify-between items-center bg-zinc-100 rounded p-2.5 shadow-lg" key={cart.id}>
              <div className="flex items-center gap-2">
                {/* TODO: Заменить на изображение товара */}
                <div className="size-32 bg-blue-200 rounded shadow-lg flex items-center justify-center text-xl">
                  <span>S3APP</span>
                </div>
                <div>
                  <div className="flex flex-col justify-between">
                    <div className="flex flex-col">
                      <span className="text-2xl font-semibold">{cart.product.name}</span>
                      {/* TODO: Добавить описание товара */}
                      {/* <span className="text-xl text-black/60 font-light">Описание</span> */}
                    </div>
                    <div className="mt-4">
                      <span className="text-2xl text-black/75 font-semibold">
                        {cart.product.price}
                        {' '}
                        ₽
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="flex items-center gap-7 mr-4">
                <div className="flex-col">
                  <div>
                    <TextField
                      variant="filled"
                      size="small"
                      label="Название"
                      defaultValue={cart.licenseKey?.id ? (cart.licenseKey?.name || 'Безымянный ключ') : cart.name || 'Безымянный ключ'}
                      onBlur={async (e) => {
                        await changeElement(cart.id, { name: e.target.value });
                      }}
                    />
                  </div>
                  <div className="flex items-center justify-between mt-2.5 text-black/60 text-base">
                    <span>
                      Ключ #
                      {index + 1}
                    </span>
                    {' '}
                    <span>
                      До
                      {' '}
                      {moment(cart.licenseKey?.expireAt
                        ? new Date(cart.licenseKey.expireAt).getTime()
                          + cart.count * cart.product.days * 24 * 60 * 60 * 1000
                        : new Date().getTime()
                          + cart.count * cart.product.days * 24 * 60 * 60 * 1000).format('DD.MM.YYYY')}
                    </span>
                  </div>
                </div>
                <div className="flex items-center gap-2">
                  <Button
                    className="min-w-0 size-9"
                    disabled={cart.count === 1}
                    color="inherit"
                    variant="contained"
                    onClick={async () => {
                      changeElement(cart.id, { count: cart.count - 1 });
                    }}
                  >
                    <Remove />
                  </Button>
                  <div className="flex flex-col items-center justify-center gap-2">
                    <span className="font-bold">
                      {cart.count}
                      {' '}
                      шт
                    </span>
                    <span>
                      {cart.count * cart.product.days}
                      {' '}
                      дней
                    </span>
                  </div>
                  <Button
                    className="min-w-0 size-9"
                    color="inherit"
                    size="small"
                    variant="contained"
                    onClick={async () => {
                      changeElement(cart.id, { count: cart.count + 1 });
                    }}
                  >
                    <Add />
                  </Button>
                </div>
                <Button
                  className="min-w-0 size-11"
                  color="inherit"
                  variant="contained"
                  onClick={async () => {
                    await removeCartElement({
                      variables: {
                        removeCartElementId: cart.id,
                      },
                    });
                    props.refetchUser();
                    refetch();
                  }}
                >
                  <DeleteOutlined />
                </Button>
              </div>
            </div>
          ))}

          <div className="flex items-center justify-end gap-4">
            <span className="text-xl font-bold">
              ИТОГО:
              {' '}
              {data.me.cart.reduce((total, item) => total + (item.product.price * item.count), 0)}
              {' '}
              ₽
            </span>
            <Button
              className="normal-case bg-primary/20 text-primary text-lg font-normal"
              size="large"
              variant="contained"
              onClick={async () => {
                await orderCart();
                props.refetchUser();
                router.push('/orders');
                refetch();
              }}
            >
              Заказать
            </Button>
            <Button
              className="min-w-0 size-11"
              color="inherit"
              variant="contained"
              onClick={async () => {
                await clearCart();
                await refetch();
              }}
            >
              <DeleteOutlined />
            </Button>
          </div>
        </div>
      )
        : (
          <div className="size-full flex flex-col items-center justify-center">
            <span
              className="text-4xl uppercase text-black/40 font-medium"
            >
              Корзина пуста
            </span>
          </div>
        )}
    </div>
  );
}

export default Cart;
