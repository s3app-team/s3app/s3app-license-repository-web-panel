import CircularProgress from '@mui/material/CircularProgress';

export default function LoadingCircle() {
  return (
    <div className="size-full flex items-center justify-center">
      <CircularProgress />
    </div>
  );
}
